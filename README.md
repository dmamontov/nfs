# nfs

```
[root@clnt vagrant]# mount | grep nfs
sunrpc on /var/lib/nfs/rpc_pipefs type rpc_pipefs (rw,relatime)
192.168.50.9:/var/nfs/ on /mnt/upload type nfs (rw,relatime,vers=3,rsize=32768,wsize=32768,namlen=255,hard,proto=udp,timeo=11,retrans=3,sec=sys,mountaddr=192.168.50.9,mountvers=3,mountport=20048,mountproto=udp,local_lock=none,addr=192.168.50.9)
```
